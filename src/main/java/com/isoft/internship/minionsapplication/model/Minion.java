package com.isoft.internship.minionsapplication.model;

/**
 * Implementation class for the minions table in the database.
 *
 * @author Deyan Georgiev
 */
public class Minion extends BaseEntity {

    private String name;
    private short age;
    private Town town;

    /**
     * Constructor for setting the minion properties.
     *
     * @param name the name of the minion.
     * @param age  the age of the minion.
     * @param town the town of the minion.
     */
    public Minion(int id, String name, short age, Town town) {
        super(id);
        this.name = name;
        this.age = age;
        this.town = town;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the age
     */
    public short getAge() {
        return age;
    }

    /**
     * @return the townId
     */
    public Town getTown() {
        return town;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param age the age to set
     */
    public void setAge(short age) {
        this.age = age;
    }

    /**
     * @param town the town to set
     */
    public void setTown(Town town) {
        this.town = town;
    }
}
