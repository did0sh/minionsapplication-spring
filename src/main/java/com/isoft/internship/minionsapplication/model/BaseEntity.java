package com.isoft.internship.minionsapplication.model;

/**
 * Abstract class which contains the id property of all the entities.
 *
 * @author Deyan Georgiev
 */
public abstract class BaseEntity {

    private int id;

    /**
     * Constructor for setting the id.
     *
     * @param id the id of the entity.
     */
    BaseEntity(int id) {
        this.id = id;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return this.id;
    }
}
