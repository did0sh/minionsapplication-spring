package com.isoft.internship.minionsapplication.model;

/**
 * Enumeration class for the superpower table column.
 *
 * @author Deyan Georgiev
 */
public enum SuperPower {
    INVISIBILITY, FLYING, HEALING, SPEED
}
