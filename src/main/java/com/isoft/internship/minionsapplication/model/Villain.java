package com.isoft.internship.minionsapplication.model;

/**
 * Villain class that represents the villains table in the database.
 *
 * @author Deyan Georgiev
 */
public class Villain extends BaseEntity {

    private String name;
    private Evilness evilness;
    private SuperPower superpower;

    /**
     * Constructor for setting the villain properties.
     *
     * @param name       the name of the villain.
     * @param evilness   the evilness of the villain.
     * @param superpower the superpower of the villain.
     */
    public Villain(int id, String name, Evilness evilness, SuperPower superpower) {
        super(id);
        this.name = name;
        this.evilness = evilness;
        this.superpower = superpower;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the evilness
     */
    public Evilness getEvilness() {
        return evilness;
    }

    /**
     * @return the superpower
     */
    public SuperPower getSuperpower() {
        return superpower;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param evilness the evilness to set
     */
    public void setEvilness(String evilness) {
        this.evilness = Evilness.valueOf(evilness.toUpperCase());
    }

    /**
     * @param superpower the superpower to set
     */
    public void setSuperpower(String superpower) {
        this.superpower = SuperPower.valueOf(superpower.toLowerCase());
    }
}
