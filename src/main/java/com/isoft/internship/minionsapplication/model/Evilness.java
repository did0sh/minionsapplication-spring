package com.isoft.internship.minionsapplication.model;

/**
 * Enumeration class for the evilness table column.
 *
 * @author Deyan Georgiev
 */
public enum Evilness {
    GOOD, BAD, EVIL, SUPEREVIL
}
