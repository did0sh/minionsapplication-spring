package com.isoft.internship.minionsapplication.mapper;

import com.isoft.internship.minionsapplication.model.Town;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper class that returns an entity row from the database.
 */
@Component
public class TownMapper implements RowMapper<Town> {
    @Override
    public Town mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Town(resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getString("country"));
    }
}
