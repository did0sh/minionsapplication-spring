package com.isoft.internship.minionsapplication.mapper;

import com.isoft.internship.minionsapplication.model.Evilness;
import com.isoft.internship.minionsapplication.model.SuperPower;
import com.isoft.internship.minionsapplication.model.Villain;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper class that returns an entity row from the database.
 */
@Component
public class VillainMapper implements RowMapper<Villain> {
    @Override
    public Villain mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Villain(resultSet.getInt("id"),
                resultSet.getString("name"),
                Evilness.valueOf(resultSet.getString("evilness").toUpperCase()),
                SuperPower.valueOf(resultSet.getString("superpower").toUpperCase()));
    }
}
