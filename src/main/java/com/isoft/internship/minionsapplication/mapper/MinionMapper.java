package com.isoft.internship.minionsapplication.mapper;

import com.isoft.internship.minionsapplication.dao.TownDao;
import com.isoft.internship.minionsapplication.model.Minion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper class that returns an entity row from the database.
 */
@Component
public class MinionMapper implements RowMapper<Minion> {

    private TownDao townDao;

    public MinionMapper(TownDao townDao) {
        this.townDao = townDao;
    }

    @Override
    public Minion mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Minion(resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getShort("age"),
                townDao.getTownById(resultSet.getInt("town_id")));
    }
}
