package com.isoft.internship.minionsapplication.service;


import com.isoft.internship.minionsapplication.model.Villain;

import java.util.List;

/**
 * Interface defining the VillainServiceImpl methods
 * @author Deyan Georgiev
 * @see VillainServiceImpl
 */
public interface VillainService {
    /**
     * Method for saving a random villain in the database.
     */
    void saveVillain();

    /**
     * Method for updating a villain in the database.
     *
     * @param villain
     */
    void updateVillain(Villain villain);

    /**
     * Method for deleting a villain in the database.
     *
     * @param id
     */
    void deleteVillain(int id);

    /**
     * Method for getting a villain by its id from the database.
     *
     * @param id
     * @return the town
     */
    Villain getVillainById(int id);

    /**
     * Method for getting all villains from the database.
     *
     * @return all the villain entities.
     */
    List<Villain> getAllVillains();
}