package com.isoft.internship.minionsapplication.service;

import com.isoft.internship.minionsapplication.model.Town;

import java.util.List;

/**
 * Interface class defining the methods for the TownServiceImpl
 *
 * @author Deyan Georgiev
 * @see TownServiceImpl
 */
public interface TownService {

    /**
     * Method for saving a random town in the database.
     */
    void saveTown();

    /**
     * Method for updating a town in the database.
     *
     * @param town
     */
    void updateTown(Town town);

    /**
     * Method for deleting a town in the database.
     *
     * @param id
     */
    void deleteTown(int id);

    /**
     * Method for getting a town by its id from the database.
     *
     * @param id
     * @return the town
     */
    Town getTownById(int id);

    /**
     * Method for getting all towns from the database.
     *
     * @return all the town entities.
     */
    List<Town> getAllTowns();

}