package com.isoft.internship.minionsapplication.service;

import com.isoft.internship.minionsapplication.dao.TownDao;
import com.isoft.internship.minionsapplication.dao.TownDaoImpl;
import com.isoft.internship.minionsapplication.model.Town;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * Service class, which main purpose is to encapsulate the logic from the townDao.
 * @author Deyan Georgiev
 * @see TownDao
 * @see TownService
 */
@Service
public class TownServiceImpl implements TownService {
    private TownDao townDao;
    private static final Random random = new Random();
    private static final Logger LOGGER = LoggerFactory.getLogger(TownServiceImpl.class);

    public TownServiceImpl(TownDao townDao) {
        this.townDao = townDao;
    }

    @Override
    public void saveTown() {
        townDao.createTown(createRandomTown());
    }

    @Override
    public void updateTown(Town town) {
        if (town != null) {
            town.setName("updTown" + town.getId());
            town.setCountry("updCountry" + town.getId());
            townDao.updateTown(town);
        } else {
            LOGGER.error("Town with this id doesn`t exist!");
        }
    }

    @Override
    public void deleteTown(int id) {
        townDao.deleteTown(id);
    }

    @Override
    public Town getTownById(int id) {
        return townDao.getTownById(id);
    }

    @Override
    public List<Town> getAllTowns() {
        return townDao.getAllTowns();
    }

    private Town createRandomTown() {
        int min = getAllTowns().size();
        int max = Integer.MAX_VALUE;
        int townId = random.nextInt((max - min) + 1) + min;
        return new Town(townId, "town" + townId, "country" + townId);
    }

}