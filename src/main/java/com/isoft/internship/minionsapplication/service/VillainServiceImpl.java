package com.isoft.internship.minionsapplication.service;

import com.isoft.internship.minionsapplication.dao.VillainDao;
import com.isoft.internship.minionsapplication.model.Evilness;
import com.isoft.internship.minionsapplication.model.SuperPower;
import com.isoft.internship.minionsapplication.model.Villain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * Service class, which main purpose is to encapsulate the logic from the villainDao.
 * @author Deyan Georgiev
 * @see VillainDao
 * @see VillainService
 */
@Service
public class VillainServiceImpl implements VillainService {
    private VillainDao villainDao;
    private static final Random random = new Random();
    private static final Logger LOGGER = LoggerFactory.getLogger(VillainServiceImpl.class);

    public VillainServiceImpl(VillainDao villainDao) {
        this.villainDao = villainDao;
    }

    @Override
    public void saveVillain() {
        villainDao.createVillain(createRandomVillain());
    }

    @Override
    public void updateVillain(Villain villain) {
        if (villain != null) {
            villain.setName("updVillain" + villain.getId());
            villain.setEvilness(getRandomEvilness().toString());
            villain.setSuperpower(getRandomSuperPower().toString());
            villainDao.updateVillain(villain);
        } else {
            LOGGER.error("Villain with this id doesn`t exist!");
        }
    }

    @Override
    public void deleteVillain(int id) {
        villainDao.deleteVillain(id);
    }

    @Override
    public Villain getVillainById(int id) {
        return villainDao.getVillainById(id);
    }

    @Override
    public List<Villain> getAllVillains() {
        return villainDao.getAllVillains();
    }

    private Villain createRandomVillain() {
        int min = getAllVillains().size();
        int max = Integer.MAX_VALUE;
        int villainId = random.nextInt((max - min) + 1) + min;
        return new Villain(villainId, "viialin" + villainId, getRandomEvilness(), getRandomSuperPower());
    }

    private Evilness getRandomEvilness() {
        return Evilness.values()[random.nextInt(Evilness.values().length)];
    }

    private SuperPower getRandomSuperPower() {
        return SuperPower.values()[random.nextInt(SuperPower.values().length)];
    }

}