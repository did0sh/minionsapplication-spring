package com.isoft.internship.minionsapplication.service;

import com.isoft.internship.minionsapplication.dao.MinionDao;
import com.isoft.internship.minionsapplication.dao.TownDao;
import com.isoft.internship.minionsapplication.model.Minion;
import com.isoft.internship.minionsapplication.model.Town;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * Implementation class which main purpose is to encapsulate the logic from the MinionDao
 * @author Deyan Georgiev
 * @see MinionDao
 * @see MinionService
 */
@Service
public class MinionServiceImpl implements MinionService {
    private MinionDao minionDao;
    private TownDao townDao;
    private static final Random random = new Random();
    private static final Logger LOGGER = LoggerFactory.getLogger(MinionServiceImpl.class);


    public MinionServiceImpl(MinionDao minionDao, TownDao townDao) {
        this.minionDao = minionDao;
        this.townDao = townDao;
    }

    @Override
    public void saveMinion() {
        minionDao.createMinion(createRandomMinion());
    }

    @Override
    public void updateMinion(Minion minion) {
        if (minion != null) {
            minion.setName("updMinion" + minion.getId());
            minion.setAge(getRandomAge());
            minion.setTown(getRandomTown());
            minionDao.updateMinion(minion);
        } else {
            LOGGER.error("Minion with this id doesn`t exist!");
        }
    }

    @Override
    public void deleteMinion(int id) {
        minionDao.deleteMinion(id);
    }

    @Override
    public Minion getMinionById(int id) {
        return minionDao.getMinionById(id);
    }

    @Override
    public List<Minion> getAllMinions() {
        return minionDao.getAllMinions();
    }

    private Minion createRandomMinion() {
        int min = getAllMinions().size();
        int max = Integer.MAX_VALUE;
        int minionId = random.nextInt((max - min) + 1) + min;
        return new Minion(minionId, "minion" + minionId, getRandomAge(), getRandomTown());
    }

    private Town getRandomTown() {
        int randomTownIndex = random.nextInt(townDao.getAllTowns().size());
        return townDao.getAllTowns().get(randomTownIndex);
    }

    private short getRandomAge() {
        return (short) random.nextInt(Short.MAX_VALUE);
    }

}