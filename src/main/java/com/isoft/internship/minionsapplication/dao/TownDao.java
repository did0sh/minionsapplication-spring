package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.model.Town;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TownDao {

    /**
     * Method for getting the current entity by its id.
     * @param id the id of the entity.
     * @return town entity.
     */
    Town getTownById(int id);

    /**
     * Method for getting all the entities.
     * @return list of town entities.
     */
    List<Town> getAllTowns();

    /**
     * Method for deleting a current entity based on its id.
     * @param id the id of the entity.
     * @return true if the entity has been deleted and false if it has not been.
     */
    boolean deleteTown(int id);

    /**
     * Method for updating a current entity.
     * @param town the entity to update.
     * @return true if the entity has been updated and false if it has not been.
     */
    boolean updateTown(Town town);

    /**
     * Method for creating a current entity.
     * @param town the entity to create.
     * @return true if it has been created and false if it has not been.
     */
    boolean createTown(Town town);
}
