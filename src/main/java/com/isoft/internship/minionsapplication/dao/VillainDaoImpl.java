package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.mapper.VillainMapper;
import com.isoft.internship.minionsapplication.model.Villain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VillainDaoImpl implements VillainDao {
    private static final String SQL_FIND_VILLAIN = "SELECT * FROM villains WHERE id = ?";
    private static final String SQL_FIND_ALL_VILLAINS = "SELECT * FROM villains";
    private static final String SQL_DELETE_VILLAIN = "DELETE FROM villains WHERE id = ?";
    private static final String SQL_UPDATE_VILLAIN = "UPDATE villains set name = ?, evilness = ?, superpower = ? WHERE  id = ?";
    private static final String SQL_INSERT_VILLAIN = "INSERT INTO villains (id, name, evilness, superpower) values (?, ?, ?, ?)";

    private static final Logger LOGGER = LoggerFactory.getLogger(VillainDaoImpl.class);
    private JdbcTemplate jdbcTemplate;

    public VillainDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Villain getVillainById(int id) {
        try{
            Villain villain = jdbcTemplate
                    .queryForObject(SQL_FIND_VILLAIN, new Object[] { id }, new VillainMapper());
            LOGGER.info("Successfully returned the villain entity!");
            return villain;
        }catch (EmptyResultDataAccessException ex){
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Villain> getAllVillains() {
        try{
            List<Villain> villains = jdbcTemplate.query(SQL_FIND_ALL_VILLAINS, new VillainMapper());
            LOGGER.info("Successfully returned the villain entities!");
            return villains;
        }catch (EmptyResultDataAccessException ex){
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public boolean deleteVillain(int id) {
        boolean isDeleted = jdbcTemplate.update(SQL_DELETE_VILLAIN, id) > 0;
        if(isDeleted){
            LOGGER.info("Successfully deleted the villain entity!");
        } else {
            LOGGER.info("Villain with this id does not exist!");
        }
        return isDeleted;
    }

    @Override
    public boolean updateVillain(Villain villain) {
        boolean isUpdated = jdbcTemplate.update(SQL_UPDATE_VILLAIN, villain.getName(), villain.getEvilness(),
                villain.getSuperpower(),
                villain.getId()) > 0;
        if(isUpdated){
            LOGGER.info("Successfully updated the villain entity!");
        } else {
            LOGGER.info("Villain could not be updated!");
        }
        return isUpdated;
    }

    @Override
    public boolean createVillain(Villain villain) {
        boolean isCreated = jdbcTemplate.update(SQL_INSERT_VILLAIN, villain.getId(),
                villain.getName(), villain.getEvilness(), villain.getSuperpower()) > 0;
        if(isCreated){
            LOGGER.info("Successfully created the villain entity!");
        } else {
            LOGGER.info("Villain could not be created!");
        }
        return isCreated;
    }
}
