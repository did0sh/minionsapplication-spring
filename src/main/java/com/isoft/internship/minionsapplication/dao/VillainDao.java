package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.model.Villain;

import java.util.List;

public interface VillainDao {

    /**
     * Method for getting the current entity by its id.
     * @param id the id of the entity.
     * @return villain entity.
     */
    Villain getVillainById(int id);

    /**
     * Method for getting all the entities.
     * @return list of villains entities.
     */
    List<Villain> getAllVillains();

    /**
     * Method for deleting a current entity based on its id.
     * @param id the id of the entity.
     * @return true if the entity has been deleted and false if it has not been.
     */
    boolean deleteVillain(int id);

    /**
     * Method for updating a current entity.
     * @param villain the entity to update.
     * @return true if the entity has been updated and false if it has not been.
     */
    boolean updateVillain(Villain villain);

    /**
     * Method for creating a current entity.
     * @param villain the entity to create.
     * @return true if it has been created and false if it has not been.
     */
    boolean createVillain(Villain villain);
}
