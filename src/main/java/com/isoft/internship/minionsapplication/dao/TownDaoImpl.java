package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.mapper.TownMapper;
import com.isoft.internship.minionsapplication.model.Town;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TownDaoImpl implements TownDao {
    private static final String SQL_FIND_TOWN = "SELECT * FROM towns WHERE id = ?";
    private static final String SQL_FIND_ALL_TOWNS = "SELECT * FROM towns";
    private static final String SQL_DELETE_TOWN = "DELETE FROM towns WHERE id = ?";
    private static final String SQL_UPDATE_TOWN = "UPDATE towns set name = ?, country = ? WHERE  id = ?";
    private static final String SQL_INSERT_TOWN = "INSERT INTO towns (id, name, country) values (?, ?, ?)";

    private static final Logger LOGGER = LoggerFactory.getLogger(TownDaoImpl.class);
    private JdbcTemplate jdbcTemplate;

    public TownDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Town getTownById(int id) {
        try{
            Town town = jdbcTemplate
                    .queryForObject(SQL_FIND_TOWN, new Object[]{id}, new TownMapper());
            LOGGER.info("Successfully returned the town entity!");
            return town;
        }catch (EmptyResultDataAccessException ex){
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Town> getAllTowns() {
        try{
            List<Town> towns = jdbcTemplate.query(SQL_FIND_ALL_TOWNS, new TownMapper());
            LOGGER.info("Successfully returned the town entities!");
            return towns;
        }catch (EmptyResultDataAccessException ex){
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public boolean deleteTown(int id) {
        boolean isDeleted = jdbcTemplate.update(SQL_DELETE_TOWN, id) > 0;
        if(isDeleted){
            LOGGER.info("Successfully deleted the town entity!");
        } else {
            LOGGER.info("Town with this id does not exist!");
        }
        return isDeleted;
    }

    @Override
    public boolean updateTown(Town town) {
        boolean isUpdated = jdbcTemplate.update(SQL_UPDATE_TOWN, town.getName(), town.getCountry(),
                town.getId()) > 0;
        if(isUpdated){
            LOGGER.info("Successfully updated the town entity!");
        } else {
            LOGGER.info("Town could not be updated!");
        }
        return isUpdated;
    }

    @Override
    public boolean createTown(Town town) {
        boolean isCreated = jdbcTemplate.update(SQL_INSERT_TOWN, town.getId(), town.getName(), town.getCountry()) > 0;
        if(isCreated){
            LOGGER.info("Successfully created the town entity!");
        } else {
            LOGGER.info("Town could not be created!");
        }
        return isCreated;
    }
}
