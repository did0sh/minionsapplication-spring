package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.model.Minion;

import java.util.List;

public interface MinionDao {

    /**
     * Method for getting the current entity by its id.
     * @param id the id of the entity.
     * @return Minion entity.
     */
    Minion getMinionById(int id);

    /**
     * Method for getting all the entities.
     * @return list of minion entities.
     */
    List<Minion> getAllMinions();

    /**
     * Method for deleting a current entity based on its id.
     * @param id the id of the entity.
     * @return true if the entity has been deleted and false if it has not been.
     */
    boolean deleteMinion(int id);

    /**
     * Method for updating a current entity.
     * @param minion the entity to update.
     * @return true if the entity has been updated and false if it has not been.
     */
    boolean updateMinion(Minion minion);

    /**
     * Method for creating a current entity.
     * @param minion the entity to create.
     * @return true if it has been created and false if it has not been.
     */
    boolean createMinion(Minion minion);
}
