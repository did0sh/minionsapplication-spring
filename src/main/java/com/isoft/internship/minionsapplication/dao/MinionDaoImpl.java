package com.isoft.internship.minionsapplication.dao;

import com.isoft.internship.minionsapplication.mapper.MinionMapper;
import com.isoft.internship.minionsapplication.model.Minion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MinionDaoImpl implements MinionDao {
    private static final String SQL_FIND_MINION = "SELECT * FROM minions WHERE id = ?";
    private static final String SQL_FIND_ALL_MINIONS = "SELECT * FROM minions";
    private static final String SQL_DELETE_MINION = "DELETE FROM minions WHERE id = ?";
    private static final String SQL_UPDATE_MINION = "UPDATE minions set name = ?, age = ?, town_id = ? WHERE  id = ?";
    private static final String SQL_INSERT_MINION = "INSERT INTO minions (id, name, age, town_id) values (?, ?, ?, ?)";

    private static final Logger LOGGER = LoggerFactory.getLogger(MinionDaoImpl.class);
    private JdbcTemplate jdbcTemplate;
    private TownDao townDao;

    public MinionDaoImpl(JdbcTemplate jdbcTemplate, TownDao townDao) {
        this.jdbcTemplate = jdbcTemplate;
        this.townDao = townDao;
    }

    @Override
    public Minion getMinionById(int id) {
        try{
            Minion minion = jdbcTemplate
                    .queryForObject(SQL_FIND_MINION, new Object[] { id }, new MinionMapper(townDao));
            LOGGER.info("Successfully returned the minion entity!");
            return minion;
        }catch (EmptyResultDataAccessException ex){
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Minion> getAllMinions() {
       try{
           List<Minion> minions = jdbcTemplate.query(SQL_FIND_ALL_MINIONS, new MinionMapper(townDao));
           LOGGER.info("Successfully returned the minion entities!");
           return minions;
       }catch (EmptyResultDataAccessException ex){
           LOGGER.error(ex.getMessage());
       }
        return null;
    }

    @Override
    public boolean deleteMinion(int id) {
        boolean isDeleted = jdbcTemplate.update(SQL_DELETE_MINION, id) > 0;
        if(isDeleted){
            LOGGER.info("Successfully deleted the minion entity!");
        } else {
            LOGGER.info("Minion with this id does not exist!");
        }
        return isDeleted;
    }

    @Override
    public boolean updateMinion(Minion minion) {
        boolean isUpdated = jdbcTemplate.update(SQL_UPDATE_MINION, minion.getName(), minion.getAge(), minion.getTown().getId(),
                minion.getId()) > 0;
        if(isUpdated){
            LOGGER.info("Successfully updated the minion entity!");
        } else {
            LOGGER.info("Minion could not be updated!");
        }
        return isUpdated;
    }

    @Override
    public boolean createMinion(Minion minion) {
        boolean isCreated = jdbcTemplate.update(SQL_INSERT_MINION, minion.getId(),
                minion.getName(), minion.getAge(), minion.getTown().getId()) > 0;
        if(isCreated){
            LOGGER.info("Successfully created the minion entity!");
        } else {
            LOGGER.info("Minion could not be created!");
        }
        return isCreated;
    }
}
