package com.isoft.internship.minionsapplication.controller;

import com.isoft.internship.minionsapplication.model.Minion;
import com.isoft.internship.minionsapplication.service.MinionService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller class which defines our endpoints and invokes the suitable methods for the requests from the minion service.
 * @see MinionService
 */
@RestController
@RequestMapping(value = "/minions")
public class MinionController {

    private MinionService minionService;

    public MinionController(MinionService minionService) {
        this.minionService = minionService;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Minion> getMinions(){
        return minionService.getAllMinions();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Minion getMinionById(@PathVariable int id){
        return minionService.getMinionById(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void saveMinion(){
        minionService.saveMinion();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteMinionById(@PathVariable int id){
        minionService.deleteMinion(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public void updateMinionById(@PathVariable int id){
       Minion currentMinion = minionService.getMinionById(id);
       minionService.updateMinion(currentMinion);
    }
}
