package com.isoft.internship.minionsapplication.controller;

import com.isoft.internship.minionsapplication.model.Villain;
import com.isoft.internship.minionsapplication.service.VillainService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller class which defines our endpoints and invokes the suitable methods for the requests from the villain service.
 * @see VillainService
 */
@RestController
@RequestMapping(value = "/villains")
public class VillainController {

    private VillainService villainService;

    public VillainController(VillainService villainService) {
        this.villainService = villainService;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Villain> getVillains(){
        return villainService.getAllVillains();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Villain getVillainById(@PathVariable int id){
        return villainService.getVillainById(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void saveVillain(){
        villainService.saveVillain();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteTownById(@PathVariable int id){
        villainService.deleteVillain(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public void updateTownById(@PathVariable int id){
        Villain currentVillain = villainService.getVillainById(id);
        villainService.updateVillain(currentVillain);
    }
}
