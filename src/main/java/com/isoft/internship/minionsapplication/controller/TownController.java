package com.isoft.internship.minionsapplication.controller;

import com.isoft.internship.minionsapplication.model.Town;
import com.isoft.internship.minionsapplication.service.TownService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller class which defines our endpoints and invokes the suitable methods for the requests from the town service.
 * @see TownService
 */
@RestController
@RequestMapping(value = "/towns")
public class TownController {

    private TownService townService;

    public TownController(TownService townService) {
        this.townService = townService;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Town> getTowns(){
        return townService.getAllTowns();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Town getTownById(@PathVariable int id){
        return townService.getTownById(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void saveTown(){
        townService.saveTown();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteTownById(@PathVariable int id){
        townService.deleteTown(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public void updateTownById(@PathVariable int id){
        Town currentTown = townService.getTownById(id);
        townService.updateTown(currentTown);
    }
}
